USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.CARD_TYPE;
GO


CREATE TABLE dbo.CARD_TYPE(
card_type_id        TINYINT            NOT NULL  IDENTITY(1,1),
card_type_name      NVARCHAR (20)      NOT NULL,

CONSTRAINT PK_CARD_TYPE PRIMARY KEY
	(
		card_type_id 
	)
);
GO