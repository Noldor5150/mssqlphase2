USE Carsharing;
GO


DROP TABLE IF EXISTS dbo.[USER];
GO


CREATE TABLE dbo.[USER](
[user_id]         INT            NOT NULL   IDENTITY(1,1),
email             NVARCHAR (80)  NOT NULL,
first_name        NVARCHAR (80)  NOT NULL,
last_name         NVARCHAR (80)  NOT NULL,
password_hash     NVARCHAR (255) NOT NULL,
phone_number      NVARCHAR (80)  NOT NULL,

CONSTRAINT PK_USER PRIMARY KEY
	(
		[user_id]  
	)
);
GO