USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.CAR_CATEGORY;
GO


CREATE TABLE dbo.CAR_CATEGORY(
car_category_id        TINYINT            NOT NULL  IDENTITY(1,1),
car_category_name      NVARCHAR (20)      NOT NULL,

CONSTRAINT PK_CAR_CATEGORY PRIMARY KEY
	(
		car_category_id  
	)
);
GO