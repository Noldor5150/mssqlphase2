USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.DEMAND;
GO

CREATE TABLE dbo.DEMAND(
demand_id        INT            NOT NULL IDENTITY(1,1),
place            NVARCHAR (250) NOT NULL,
date_time        DATETIME       NOT NULL,
text_description NVARCHAR (280) NOT NULL,
time_stamp       DATETIME       NOT NULL,
[user_id]        INT            NOT NULL,
car_category_id  TINYINT        NOT NULL,
moderator_id     TINYINT,

CONSTRAINT PK_DEMAND PRIMARY KEY
	(
		demand_id 
	)

);
GO