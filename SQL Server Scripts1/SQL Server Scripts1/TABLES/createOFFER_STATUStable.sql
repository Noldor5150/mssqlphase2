USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.OFFER_STATUS;
GO


CREATE TABLE dbo.OFFER_STATUS(
offer_status_id        TINYINT            NOT NULL  IDENTITY(1,1),
offer_status_name      NVARCHAR (20)      NOT NULL,

CONSTRAINT PK_OFFER_STATUS PRIMARY KEY
	(
		offer_status_id  
	)
);
GO