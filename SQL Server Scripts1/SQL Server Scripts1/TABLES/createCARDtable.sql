USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.CARD;
GO

CREATE TABLE dbo.CARD(
card_id          INT            NOT NULL IDENTITY(1,1),
card_number      NVARCHAR (16)  NOT NULL,
expiration_date  DATETIME       NOT NULL,
holders_name     NVARCHAR (30)  NOT NULL,
[user_id]        INT            NOT NULL,
card_type_id     TINYINT        NOT NULL,

CONSTRAINT PK_CARD PRIMARY KEY
	(
		card_id
	)
);
GO