USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.OFFER;
GO

CREATE TABLE dbo.OFFER(
offer_id              INT             NOT NULL IDENTITY(1,1),
return_address        NVARCHAR (100)  NOT NULL,
pick_up_address       NVARCHAR (100)  NOT NULL,
date_time             DATETIME        NOT NULL,
text_description      NVARCHAR (250)  NOT NULL,
offer_owner_rating    TINYINT,
offer_consumer_rating TINYINT,
price_EU_hour         SMALLINT        NOT NULL,
[user_id]             INT             NOT NULL,
consumer_id           INT,            
offer_status_id       TINYINT         NOT NULL,
car_id                INT             NOT NULL,
moderator_id          TINYINT,

CONSTRAINT PK_OFFER PRIMARY KEY
	(
		offer_id 
	)
);
GO