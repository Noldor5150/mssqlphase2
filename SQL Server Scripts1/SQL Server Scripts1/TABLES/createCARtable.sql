USE Carsharing;
GO


DROP TABLE IF EXISTS dbo.CAR;
GO

CREATE TABLE dbo.CAR(
car_id               INT            NOT NULL      IDENTITY(1,1),
license_plate_number NVARCHAR(10)   NOT NULL,
car_brand            NVARCHAR (20)  NOT NULL,
car_model            NVARCHAR(100)  NOT NULL,
production_date      DATETIME       NOT NULL,
text_description     NVARCHAR (255) NOT NULL,
image_link           NVARCHAR (500) NOT NULL,
[user_id]            INT            NOT NULL,
car_category_id      TINYINT        NOT NULL,
fuel_type_id         TINYINT        NOT NULL,
validation_status_id TINYINT        NOT NULL,
moderator_id         TINYINT,

CONSTRAINT PK_CAR PRIMARY KEY
	(
		car_id 
	)
);
GO