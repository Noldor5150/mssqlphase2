USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.FUEL_TYPE;
GO


CREATE TABLE dbo.FUEL_TYPE(
fuel_type_id        TINYINT            NOT NULL  IDENTITY(1,1),
fuel_type_name      NVARCHAR (20)      NOT NULL,

CONSTRAINT PK_FUEL_TYPE PRIMARY KEY
	(
		fuel_type_id  
	)
);
GO