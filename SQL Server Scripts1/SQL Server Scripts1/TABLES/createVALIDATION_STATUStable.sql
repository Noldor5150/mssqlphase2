USE Carsharing;
GO

DROP TABLE IF EXISTS dbo.VALIDATION_STATUS;
GO


CREATE TABLE dbo.VALIDATION_STATUS(
validation_status_id        TINYINT           NOT NULL  IDENTITY(1,1),
validation_status_name      NVARCHAR (20)     NOT NULL,

CONSTRAINT PK_VALIDATION_STATUS PRIMARY KEY
	(
		validation_status_id 
	)
);
GO