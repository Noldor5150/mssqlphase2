USE Carsharing
GO

/*
Execution example:

	exec usp_CancelDemand @json =N'{
  "user": {
    "email": "something@gmail.com"
  },
  "demand": {
    "timeStamp": "2021-05-29 00:00:00.000"
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_CancelDemand
GO

CREATE PROCEDURE usp_CancelDemand
		@json NVARCHAR(MAX)
AS
BEGIN
          DELETE dbo.DEMAND
		  FROM OPENJSON(@json) 
			WITH 
			(
				email                 NVARCHAR (80)   '$.user.email',
			    [timeStamp]           DATETIME        '$.demand.timeStamp'
			)  
			AS c 
			JOIN dbo.[USER] ON c.email = dbo.[USER].email
			JOIN dbo.DEMAND ON dbo.[USER].[user_id] = dbo.DEMAND.[user_id]   
		  WHERE c.[timeStamp]   = dbo.DEMAND.time_stamp
				
END
GO