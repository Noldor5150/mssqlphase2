USE Carsharing
GO

/*
Execution example:

	exec usp_AddCardType @json =N'{
	"cardType": {
		"name": "visa"
	}
}'
*/

DROP PROCEDURE IF EXISTS usp_AddCardType
GO

CREATE PROCEDURE usp_AddCardType
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.CARD_TYPE
	        (card_type_name)
		  SELECT c.[name]
		  FROM OPENJSON(@json) 
			WITH 
			( [name]   NVARCHAR (20)  '$.cardType.name') AS c 
			LEFT JOIN dbo.CARD_TYPE ON c.[name] = dbo.CARD_TYPE.card_type_name
		  WHERE dbo.CARD_TYPE.card_type_name is NULL 
END
GO