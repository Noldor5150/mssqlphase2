USE Carsharing
GO

/*
Execution example:

	exec usp_AddCarCategory @json =N'{
	"carCategory": {
		"name": "luxury"
	}
}'
*/

DROP PROCEDURE IF EXISTS usp_AddCarCategory
GO

CREATE PROCEDURE usp_AddCarCategory
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.CAR_CATEGORY
	      (car_category_name)
		  SELECT c.[name]
		  FROM OPENJSON(@json) 
			WITH 
			( [name]   NVARCHAR (20)  '$.carCategory.name') AS c 
			LEFT JOIN dbo.CAR_CATEGORY ON c.[name] = dbo.CAR_CATEGORY.car_category_name
		  WHERE dbo.CAR_CATEGORY.car_category_name is NULL 
END
GO