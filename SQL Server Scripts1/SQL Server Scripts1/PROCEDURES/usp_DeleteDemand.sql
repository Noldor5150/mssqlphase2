USE Carsharing
GO

/*
Execution example:

	exec usp_DeleteDemand @json =N'{
  "moderator": {
    "email": "goodg@gmail.com"
  },
  "demand": {
    "timeStamp": "2021-05-29 00:00:00.000",
    "user": {
      "email": "something@gmail.com"
    }
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_DeleteDemand 
GO

CREATE PROCEDURE usp_DeleteDemand 
		@json NVARCHAR(MAX)
AS
BEGIN
          DELETE dbo.DEMAND
		  FROM OPENJSON(@json) 
			WITH 
			(   moderatorEmail        NVARCHAR (80)   '$.moderator.email',
				userEmail             NVARCHAR (80)   '$.demand.user.email',
			    [timeStamp]           DATETIME        '$.demand.timeStamp'
			)  
			AS c JOIN dbo.[USER] ON c.userEmail = dbo.[USER].email
			     JOIN dbo.DEMAND ON dbo.[USER].[user_id] = dbo.DEMAND.[user_id]
				 JOIN dbo.MODERATOR ON c.moderatorEmail = dbo.MODERATOR.moderator_id
		  WHERE c.[timeStamp]   = dbo.DEMAND.time_stamp 
		         AND dbo.MODERATOR.moderator_id IS NOT NULL
				
END
GO