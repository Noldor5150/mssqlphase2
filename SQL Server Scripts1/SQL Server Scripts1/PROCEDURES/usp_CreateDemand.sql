USE Carsharing
GO

/*
Execution example:

	exec usp_CreateDemand @json =N'{
  "demand": {
    "dateTime": "06/29/2022",
    "description": "need a nice fast car  for a bank  robery",
    "place": "somewhere far beyond",
    "carCategory": "luxury",
    "timeStamp": "2021-05-29 00:00:00.000"
  },
  "user": {
    "email": "something@gmail.com"
  }

}'
*/
DROP PROCEDURE IF EXISTS usp_CreateDemand
GO


CREATE PROCEDURE usp_CreateDemand
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.DEMAND
	      (
               [user_id],
               place,
               date_time,
               text_description,
               time_stamp,
               car_category_id 
          )

		  SELECT TOP 1

			   dbo.[USER].[user_id],
			   c.place,
			   c.[dateTime], 
			   c.[description], 
			   [timeStamp],
			   dbo.CAR_CATEGORY.car_category_id
			   
		  FROM OPENJSON(@json) 
			WITH 
			(
				email                 NVARCHAR (80)   '$.user.email',
			    [dateTime]            DATETIME        '$.demand.dateTime',
                [description]         NVARCHAR (280)  '$.demand.description',
                place                 NVARCHAR (250)  '$.demand.place',
                [timeStamp]           DATETIME        '$.demand.timeStamp',
                carCategory           NVARCHAR (20)   '$.demand.carCategory'

			)  AS c 
			JOIN dbo.[USER] ON c.email = dbo.[USER].email
			JOIN dbo.CAR_CATEGORY ON c.carCategory   = dbo.CAR_CATEGORY.car_category_name
		    JOIN dbo.[CARD] ON dbo.[USER].[user_id] = dbo.[CARD].[user_id]
		    LEFT JOIN dbo.DEMAND ON dbo.[USER].[user_id] = dbo.DEMAND.[user_id] 
				 AND c.[timeStamp] = dbo.DEMAND.[time_stamp]
			WHERE dbo.DEMAND.[time_stamp] is NULL;
					
END
GO