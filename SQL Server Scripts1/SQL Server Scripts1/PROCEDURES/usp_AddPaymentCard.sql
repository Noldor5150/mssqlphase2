USE Carsharing
GO

/*
Execution example:

	exec usp_AddPaymentCard @json =N'{
  "card": {
    "cardNumber": "12322132132131",
    "ownersName": "Paulaz Zlyden",
    "cardType": "Visa",
    "expirationDate": "05/06/2222"
  },
    "user": {
        "email": "something@gmail.com"
    }
}'
*/
DROP PROCEDURE IF EXISTS usp_AddPaymentCard
GO


CREATE PROCEDURE usp_AddPaymentCard
	@json NVARCHAR(MAX)
AS
BEGIN
			INSERT dbo.[CARD]
			(
			   [user_id],
               card_number,        
               expiration_date,       
               holders_name ,     
               card_type_id             
			)

			SELECT
			   dbo.[USER].[user_id],
			   c.cardNumber, 
			   c.expirationDate, 
			   c.ownersName , 
			   dbo.CARD_TYPE.card_type_id
			
		    FROM OPENJSON(@json) 
			  WITH 
			  (
				email                 NVARCHAR(80)   '$.user.email',
				cardNumber            NVARCHAR(16)   '$.card.cardNumber',
				ownersName            NVARCHAR(30)   '$.card.ownersName',
				cardType              NVARCHAR(30)   '$.card.cardType',
				expirationDate        DATETIME       '$.card.expirationDate'
			  )  
			   AS c 
			   JOIN dbo.[USER] ON c.email = dbo.[USER].email
			   JOIN dbo.CARD_TYPE ON c.cardType   = dbo.CARD_TYPE.card_type_name
			   LEFT JOIN dbo.[CARD] ON c.cardNumber  = dbo.[CARD].card_number
			WHERE dbo.[CARD].card_number is NULL
END
GO