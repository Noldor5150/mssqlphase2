USE Carsharing
GO

/*
Execution example:

	exec usp_AddOfferStatus @json =N'{
	"offerStatus": {
		"name": "created"
	}
}'
*/

DROP PROCEDURE IF EXISTS usp_AddOfferStatus
GO

CREATE PROCEDURE usp_AddOfferStatus 
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.OFFER_STATUS
	        (offer_status_name)
		  SELECT c.[name]
		  FROM OPENJSON(@json) 
			WITH 
			( [name]   NVARCHAR (20)  '$.offerStatus.name') AS c 
			LEFT JOIN dbo.OFFER_STATUS ON c.[name] = dbo.OFFER_STATUS.offer_status_name
		  WHERE dbo.OFFER_STATUS.offer_status_name is NULL 
END
GO