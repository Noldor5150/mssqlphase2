USE Carsharing
GO

/*
Execution example:

	exec usp_AddValidationStatus @json =N'{
	"validationStatus": {
		"name": "created"
	}
}'
*/

DROP PROCEDURE IF EXISTS usp_AddValidationStatus 
GO

CREATE PROCEDURE usp_AddValidationStatus 
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.VALIDATION_STATUS
	        (validation_status_name)
		  SELECT c.[name]
		  FROM OPENJSON(@json) 
			WITH 
			( [name]   NVARCHAR (20)  '$.validationStatus.name') AS c 
			LEFT JOIN dbo.VALIDATION_STATUS ON c.[name] = dbo.VALIDATION_STATUS.validation_status_name
		  WHERE dbo.VALIDATION_STATUS.validation_status_name is NULL 
END
GO