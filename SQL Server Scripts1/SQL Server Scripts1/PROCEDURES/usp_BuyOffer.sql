USE Carsharing
GO

/*
Execution example:

	exec usp_BuyOffer @json =N'{
  "user": {
    "email": "paulius@gmail.com"
  },
  "offer": {
    "dateTime": "2021-05-29 00:00:00.000"
  },
  "car": {
    "licensePlateNumber": "ARZ777"
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_BuyOffer 
GO


CREATE PROCEDURE usp_BuyOffer 
		@json NVARCHAR(MAX)
AS
BEGIN
          UPDATE dbo.OFFER
		  
		  SET 
		       offer_status_id   = dbo.ufn_getBoughtOfferStatusID(),
		       consumer_id = dbo.[USER].[user_id]
			   
		  FROM OPENJSON(@json) 
			WITH 
			(
				email                 NVARCHAR (80)   '$.user.email',
			    [dateTime]            DATETIME        '$.offer.dateTime',
				licensePlateNumber    NVARCHAR (12)   '$.car.licensePlateNumber'
			)  
			AS c JOIN dbo.[USER] ON c.email = dbo.[USER].email
			     JOIN dbo.CAR ON c.licensePlateNumber = dbo.CAR.license_plate_number
				 JOIN dbo.[CARD] ON dbo.[USER].[user_id] = dbo.[CARD].[user_id]
				 JOIN dbo.OFFER ON dbo.CAR.[user_id] = dbo.OFFER.[user_id] 
				     AND dbo.OFFER.date_time = c.[dateTime] 
				     AND dbo.CAR.car_id = dbo.OFFER.car_id
			     JOIN dbo.OFFER_STATUS  AS currentStatus ON  currentStatus.offer_status_id = OFFER.offer_status_id
				 
		   WHERE
		         currentStatus.offer_status_id  = dbo.ufn_getCreatedOfferStatusID()
				 AND dbo.[USER].[user_id] <> dbo.CAR.[user_id]
				
		   
					
END
GO