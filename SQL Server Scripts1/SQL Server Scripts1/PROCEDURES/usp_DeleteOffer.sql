USE Carsharing;
GO
/*
Execution example:

	exec usp_DeleteOffer @json =N'{
  "moderator": {
    "email": "goodg@gmail.com"
  },
  "offer": {
    "dateTime": "2021-05-29 00:00:00.000"
  },
  "user": {
    "email": "something@gmail.com"
  },
  "car": {
    "licensePlateNumber": "ARZ777"
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_DeleteOffer
GO


CREATE PROCEDURE usp_DeleteOffer
	@json NVARCHAR(MAX)
AS
BEGIN
          UPDATE dbo.OFFER
		  
		  SET  offer_status_id   = dbo.ufn_getDeletedOfferStatusID(),
		       moderator_id = dbo.MODERATOR.moderator_id
			   
		  FROM OPENJSON(@json) 
			WITH 
			(
			    moderatorEmail        NVARCHAR (30)   '$.moderator.email',
				userEmail             NVARCHAR (30)   '$.user.email',
			    [dateTime]            DATETIME        '$.offer.dateTime',
				licensePlateNumber    NVARCHAR (12)   '$.car.licensePlateNumber'
			)  
			AS c JOIN dbo.[USER] ON c.userEmail = dbo.[USER].email
			     JOIN dbo.MODERATOR ON c.moderatorEmail = dbo.MODERATOR.email
			     JOIN dbo.CAR ON c.licensePlateNumber = dbo.CAR.license_plate_number
			     JOIN dbo.OFFER ON dbo.[USER].[user_id] = dbo.OFFER.[user_id] 
				      AND c.[dateTime] = dbo.OFFER.date_time 
				      AND dbo.CAR.car_id = dbo.OFFER.car_id
			     JOIN dbo.OFFER_STATUS  AS currentStatus ON  currentStatus.offer_status_id = OFFER.offer_status_id 
		   WHERE currentStatus.offer_status_id <> dbo.ufn_getBoughtOfferStatusID() 
		         AND dbo.MODERATOR.moderator_id IS NOT NULL 
				
		   
					
END
GO