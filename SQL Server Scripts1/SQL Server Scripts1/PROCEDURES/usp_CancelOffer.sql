USE Carsharing
GO

/*
Execution example:

	exec usp_CancelOffer @json =N'{
  "user": {
    "email": "something@gmail.com"
  },
  "offer": {
    "dateTime": "2021-05-29 00:00:00.000"
  },
  "car": {
    "licensePlateNumber": "ARZ777"
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_CancelOffer
GO


CREATE PROCEDURE usp_CancelOffer
	@json NVARCHAR(MAX)
AS
BEGIN
          UPDATE dbo.OFFER
		  
		  SET  offer_status_id   = dbo.ufn_getCanceledOfferStatusID()
			   
		  FROM OPENJSON(@json) 
			WITH 
			(
				email                 NVARCHAR (80)   '$.user.email',
			    [dateTime]            DATETIME        '$.offer.dateTime',
				licensePlateNumber    NVARCHAR (12)   '$.car.licensePlateNumber'
			)  
			AS c 
			JOIN dbo.[USER] ON c.email = dbo.[USER].email
		    JOIN dbo.CAR ON c.licensePlateNumber = dbo.CAR.license_plate_number
			JOIN dbo.OFFER ON dbo.[USER].[user_id] = dbo.OFFER.[user_id]
			    AND c.[dateTime] = dbo.OFFER.date_time 
			    AND dbo.CAR.car_id = dbo.OFFER.car_id
			JOIN dbo.OFFER_STATUS  AS currentStatus ON  currentStatus.offer_status_id = OFFER.offer_status_id 
		  WHERE currentStatus.offer_status_name <> dbo.ufn_getBoughtOfferStatusID()
				
		   
					
END
GO