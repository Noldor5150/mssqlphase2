USE Carsharing
GO

/*
Execution example:

	exec usp_OfferOwnerRatingEvaluation @json =N'{
  "consumer": {
    "email": "paulius@gmail.com"
  },
  "offer": {
    "dateTime": "2021-05-29 00:00:00.000",
    "owner": {
      "email": "something@gmail.com",
      "offerOwnerRating": 1
    },
    "car": {
      "licensePlateNumber": "ARZ777"
    }
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_OfferOwnerRatingEvaluation
GO


CREATE PROCEDURE usp_OfferOwnerRatingEvaluation
	@json NVARCHAR(MAX)
AS
BEGIN
          UPDATE dbo.OFFER
		  
		  SET  
		  offer_owner_rating = c.offerOwnerRating
		  

			 
		  FROM OPENJSON(@json) 
			WITH 
			(
				consumerEmail         NVARCHAR (80)   '$.consumer.email',
				ownerEmail            NVARCHAR (80)   '$.offer.owner.email',
				offerOwnerRating      TINYINT         '$.offer.owner.offerOwnerRating',
			    [dateTime]            DATETIME        '$.offer.dateTime',
				licensePlateNumber    NVARCHAR (12)   '$.offer.car.licensePlateNumber'
			)  
			AS c JOIN dbo.[USER] ON c.ownerEmail = dbo.[USER].email
			     JOIN dbo.[USER] AS consumer ON c.consumerEmail  = consumer.email
			     JOIN dbo.CAR ON c.licensePlateNumber = dbo.CAR.license_plate_number
			     JOIN dbo.OFFER ON dbo.[USER].[user_id] = dbo.OFFER.[user_id] 
				     AND c.[dateTime] = dbo.OFFER.date_time 
				     AND dbo.CAR.car_id = dbo.OFFER.car_id
				     AND dbo.OFFER.consumer_id = consumer.[user_id]
			     JOIN dbo.OFFER_STATUS  AS currentStatus ON  currentStatus.offer_status_id = OFFER.offer_status_id 
		   WHERE currentStatus.offer_status_id = dbo.ufn_getFinishedOfferStatusID() 
				
		   
	
END
GO
