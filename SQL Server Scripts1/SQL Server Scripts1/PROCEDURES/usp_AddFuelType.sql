USE Carsharing
GO

/*
Execution example:

	exec usp_AddFuelType @json =N'{
	"fuelType": {
		"name": "diesel"
	}
}'
*/

DROP PROCEDURE IF EXISTS usp_AddFuelType
GO

CREATE PROCEDURE usp_AddFuelType 
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.FUEL_TYPE
	        (fuel_type_name)
		  SELECT c.[name]
		  FROM OPENJSON(@json) 
			WITH 
			( [name]   NVARCHAR (20)  '$.fuelType.name') AS c 
			LEFT JOIN dbo.FUEL_TYPE ON c.[name] = dbo.FUEL_TYPE.fuel_type_name
		  WHERE dbo.FUEL_TYPE.fuel_type_name is NULL 
END
GO