USE Carsharing
GO

/*
Execution example:

	exec usp_Login @json =N' "user": {
    "email": "something@gmail.com",
    "passwordHash": "65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5"
  }'
*/
DROP PROCEDURE IF EXISTS usp_Login 
GO
CREATE PROCEDURE usp_Login
		@json NVARCHAR(MAX)
AS
BEGIN

IF EXISTS
    (
    SELECT dbo.[USER].[user_id]
	
	FROM
		  OPENJSON(@json) 
			WITH 
			(
				email          NVARCHAR (80)   '$.user.email',
				passwordHash   NVARCHAR (250)  '$.user.passwordHash'
			)  
			AS u
			WHERE dbo.[USER].email = u.email AND dbo.[USER].password_hash = u.passwordHash

    UNION

	SELECT dbo.MODERATOR.moderator_id

	FROM OPENJSON(@json) 
			WITH 
			(
				email          NVARCHAR (80)   '$.moderator.email',
				passwordHash   NVARCHAR (250)  '$.moderator.passwordHash'
			)  
			AS m
	WHERE dbo.MODERATOR.email = m.email AND dbo.MODERATOR.password_hash = m.passwordHash
    )
	
	 
    BEGIN
        RETURN 1
    END
ELSE
    BEGIN
        RETURN 0
    END
END
GO