USE Carsharing
GO

/*
Execution example:

	exec usp_ValidateCar @json =N'{
  "moderator": {
    "email": "last@gmail.com"
  },
  "car": {
    "licensePlateNumber": "BBD777" 
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_ValidateCar
GO


CREATE PROCEDURE usp_ValidateCar
	@json NVARCHAR(MAX)
AS
BEGIN
          UPDATE dbo.CAR
		  
		  SET 
		  moderator_id  = dbo.MODERATOR.moderator_id ,
		  validation_status_id = acceptedValidationStatus.validation_status_id
			   
		  FROM OPENJSON(@json) 
			WITH 
			(
				email                 NVARCHAR (80)   '$.moderator.email',
			    licensePlateNumber    NVARCHAR (12)   '$.car.licensePlateNumber'
			)  
			AS c 
			JOIN dbo.MODERATOR ON c.email = dbo.MODERATOR.email
			CROSS JOIN 
				 (SELECT validation_status_id  FROM  dbo.VALIDATION_STATUS WHERE 'accepted'   = VALIDATION_STATUS.validation_status_name ) 
				 AS  acceptedValidationStatus
				 
		  WHERE dbo.CAR.license_plate_number = c.licensePlateNumber  ;
					
END
GO