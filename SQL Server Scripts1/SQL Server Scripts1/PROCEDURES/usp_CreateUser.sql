


USE Carsharing
GO
/*
Execution example:

	exec usp_CreateUser @json =N'{
	"user": {
		"name": "something",
		"surname": "something",
		"email": "something@gmail.com",
		"phoneNumber": "+3706989898",
		"password": "65e84beasds87987as879c48129675f9eff3a682b27168c0ea744b2cf58ee98745b325"
	}
}'
*/

DROP PROCEDURE IF EXISTS dbo.usp_CreateUser
GO

CREATE PROCEDURE dbo.usp_CreateUser
	@json NVARCHAR(MAX)
AS
BEGIN

	INSERT dbo.[USER] 
	(
	  email, 
	  first_name,
	  last_name,
	  password_hash,
	  phone_number
	)

	SELECT 
	  user_cte.email, 
	  user_cte.[name], 
	  user_cte.surname, 
	  user_cte.passwordHash,
	  user_cte.phoneNumber
	
	FROM OPENJSON(@json)
			WITH 
			(
				[name]       NVARCHAR(80)  '$.user.name',
				surname      NVARCHAR(80)  '$.user.surname',
				email        NVARCHAR(80)  '$.user.email',
				passwordHash VARCHAR(255)  '$.user.password',
				phoneNumber  NVARCHAR(80)  '$.user.phoneNumber'
			) 
			user_cte
	        LEFT JOIN dbo.[USER] AS u
	        ON user_cte.email = u.email
	WHERE u.email is NULL
		
			
END
GO
