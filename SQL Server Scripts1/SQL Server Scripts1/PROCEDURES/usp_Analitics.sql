USE Carsharing
GO

/*
Execution example:

	exec usp_Analitics @json =N'{
  "dateTime": {
    "start": "05/29/2021",
    "end": "08/29/2021"
  },
  "carCategory":[
          "luxury",
          "economy"
           ]
}'
*/

DROP PROCEDURE IF EXISTS usp_Analitics
GO

CREATE PROCEDURE usp_Analitics
	@json NVARCHAR(MAX)
AS
BEGIN
DROP TABLE IF EXISTS #json_params
	   CREATE TABLE #json_params
       (
         timeStart          DATETIME ,
         timeEnd            DATETIME ,
         carCategories      NVARCHAR(20)
       )

	   INSERT INTO #json_params
	   (
	     timeStart,
	     timeEnd,
	     carCategories 
	   )
	   SELECT
	       COALESCE (js.startDate, (SELECT MIN(date_time) FROM dbo.OFFER)),
	       COALESCE (js.endDate , GETDATE()),
	       jss.carCategory 
	
	   FROM
	      OPENJSON(@json)
		   
			  WITH 
			 (
			    startDate             DATETIME        '$.dateTime.start ',
				endDate               DATETIME        '$.dateTime.end ',
				carCategory           NVARCHAR(max)   '$.carCategory' as json
			 ) 
			   js 
			   OUTER APPLY OPENJSON(js.carCategory)
					WITH 
					(
						carCategory 		NVARCHAR(20)	'$'
					) 
					jss;
  
     IF NOT EXISTS  (SELECT * FROM #json_params)
           INSERT INTO #json_params
	     (
	         timeStart,
	         timeEnd
	     )

	   SELECT
	      (SELECT MIN(date_time) FROM dbo.OFFER),
	      GETDATE()

DROP TABLE IF EXISTS #json_params2
	  CREATE TABLE #json_params2
        (
          timeStart          DATETIME ,
          timeEnd            DATETIME ,
          carCategories      NVARCHAR(20)
        )
	  IF EXISTS
	    (
	      SELECT * FROM #json_params
	      WHERE carCategories IS NOT NULL
	    )
	    INSERT INTO #json_params2
	     (
	      timeStart,
	      timeEnd,
	      carCategories 
	    )

	    SELECT
	      timeStart,
	      timeEnd,
	      carCategories 
	    FROM 
	      #json_params
	  ELSE
	   INSERT INTO #json_params2
	   (
	      timeStart,
	      timeEnd,
	      carCategories 
	   )
	   SELECT
	      #json_params.timeStart,
	      #json_params.timeEnd ,
	      dbo.CAR_CATEGORY.car_category_name
	   
	   FROM 
	      #json_params
	      CROSS JOIN dbo.CAR_CATEGORY;

DROP TABLE IF EXISTS #stats1
	CREATE TABLE #stats1
	(
	   [dateTime] VARCHAR(10),
	   car_category_name VARCHAR (10),
	   [name] VARCHAR (10),
	   surName VARCHAR (10),
	   [count] INT
	)
	; WITH cte_OFFER_CAR_CATEGORY AS

       (
          SELECT
             dbo.OFFER.date_time,
			 CAST(DATEPART(YEAR,dbo.OFFER.date_time) AS VARCHAR (10)) +'.'+ CAST(DATEPART(MONTH,dbo.OFFER.date_time) 
		        AS VARCHAR (10)) AS varCharDate, 
             dbo.OFFER.offer_id,
	         dbo.[USER].first_name,
             dbo.[USER].last_name,
             dbo.CAR_CATEGORY.car_category_name
		   FROM
		   dbo.OFFER
		       JOIN dbo.[USER] ON dbo.OFFER.[user_id] = dbo.[USER].[user_id]
               JOIN dbo.CAR ON dbo.[OFFER].[car_id] = dbo.CAR.[car_id]
               JOIN dbo.CAR_CATEGORY ON dbo.CAR_CATEGORY.[car_category_id] = dbo.CAR.car_category_id
			   JOIN #json_params2
               ON dbo.CAR_CATEGORY.car_category_name  = #json_params2.carCategories
		       WHERE dbo.OFFER.date_time 
			      BETWEEN #json_params2.timeStart AND #json_params2.timeEnd
	   ) 

	INSERT INTO  #stats1
	( 
	   [dateTime],
	   car_category_name,
	   [name],
	   surName,
	   [count] 
	)
		
   SELECT  
        varCharDate, 
	    car_category_name,
		first_name,
		last_name,
		COUNT (last_name) AS offers_count_by_surname
   FROM 
        cte_OFFER_CAR_CATEGORY
   GROUP BY 
        varCharDate, 
	    car_category_name,
	    first_name,
	    last_name
   
DROP TABLE IF EXISTS #time_table
	CREATE TABLE #time_table
		
	(
	   Year_Month VARCHAR (10)
	)
       ; WITH cte_TimeVariables (timeStart, timeEnd) AS  (SELECT TOP (1) timeStart, timeEnd FROM #json_params2),

          cte_Date (time_period ) AS
        (
              SELECT  timeStart 
			  FROM    cte_TimeVariables 
			  UNION ALL
              SELECT  DATEADD(MONTH, 1, time_period)
              FROM    cte_Date CROSS JOIN cte_TimeVariables
              WHERE   DATEADD(MONTH, 1, time_period) <= timeEnd
        )

	INSERT INTO  #time_table
	(
	   Year_Month
	)
       SELECT  CAST(DATEPART(YEAR,time_period) AS VARCHAR (10)) +'.'+ CAST(DATEPART(MONTH,time_period) 
		        AS VARCHAR (10)) 
		        AS new_date_time
       FROM    cte_Date
       OPTION  (MaxRecursion 100)

    DROP TABLE IF EXISTS #pivot_data
	CREATE TABLE #pivot_data
	(
	   [dateTime] VARCHAR(10),
	   car_category_name VARCHAR (10),
	   name_surname VARCHAR (30),
	   [count] INT
	)
    INSERT INTO  #pivot_data
	( 
	   [dateTime],
	   car_category_name,
	   name_surname,
	   [count] 
	)
	SELECT
	   #time_table.Year_Month,
	   #stats1.car_category_name,
	   #stats1.[name] +' '+ #stats1.surName,
	   #stats1.[count]
    FROM
	   #time_table LEFT JOIN #stats1 ON  #time_table.Year_Month = #stats1.[dateTime]
	
	DROP TABLE IF EXISTS #pivot_data2
	CREATE TABLE #pivot_data2
	(
	   [dateTime] VARCHAR(10),
	   car_category_name VARCHAR (10),
	   name_surname_count VARCHAR (50),
	
	)
	;WITH cte_Max_Count AS
      (
          SELECT
            #pivot_data.[dateTime],
			#pivot_data.car_category_name,
			MAX([count]) AS max_count 
		
		  FROM
		   #pivot_data
		  GROUP BY car_category_name,[dateTime]
	  ) 
	INSERT INTO  #pivot_data2
	( 
	   [dateTime],
	   car_category_name,
	   name_surname_count
	)
		
   SELECT  
        #pivot_data.[dateTime],
	    #json_params2.carCategories,
	    #pivot_data.name_surname +'-'+ CAST((#pivot_data.[count]) AS VARCHAR (3))
   FROM 
        #json_params2 
		LEFT JOIN #pivot_data ON  #json_params2.carCategories = #pivot_data.car_category_name
		LEFT JOIN cte_Max_Count ON  #pivot_data.[count] = cte_Max_Count.max_count 
		     AND #pivot_data.car_category_name = cte_Max_Count.car_category_name
		     AND #pivot_data.[dateTime] = cte_Max_Count.[dateTime]
		ORDER BY [dateTime]
	
	DECLARE @Pivot_Column [NVARCHAR](MAX);  
    DECLARE @Query [NVARCHAR](MAX); 
	SELECT  @Pivot_Column = STRING_AGG (Year_Month, '],[')  FROM #time_table
    SELECT  @Query='SELECT car_category_name,['+@Pivot_Column+']
         FROM   
         #pivot_data2   
         PIVOT (MAX([name_surname_count]) 
            FOR [dateTime] in (['+@Pivot_Column+'])) AS pvt
            ORDER BY car_category_name' 
	 EXEC (@Query)
END
GO