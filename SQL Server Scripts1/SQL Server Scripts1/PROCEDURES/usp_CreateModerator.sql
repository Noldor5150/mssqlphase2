USE Carsharing
GO
/*
Execution example:

	exec usp_CreateModerator @json =N'{
	"moderator": {
		"userName": "something",
		"email": "somethin7g@gmail.com",
		"password": "65e84beasds87987as879c48129675f9eff3a682b27168c0ea744b2cf58ee98745b325"
	}
}'
*/

DROP PROCEDURE IF EXISTS usp_CreateModerator
GO

CREATE PROCEDURE usp_CreateModerator
	@json NVARCHAR(MAX)
AS
BEGIN

	INSERT dbo.MODERATOR
	(
	   userName,
       password_hash ,
       email        
	)

	SELECT 
	  moderator_cte.userName , 
	  moderator_cte.passwordHash, 
	  moderator_cte.email
	
	FROM OPENJSON(@json)
			WITH 
			(
				userName       NVARCHAR(80)  '$.moderator.userName',
				email          NVARCHAR(80)  '$.moderator.email',
				passwordHash   VARCHAR(255)  '$.moderator.password'
			) 
			moderator_cte
	       LEFT JOIN dbo.MODERATOR AS m
	       ON moderator_cte.email = m.email
	WHERE m.email is NULL
			
END
GO
