USE Carsharing
GO

/*
Execution example:

	exec usp_AddCar @json =N'{
  "car": {
    "carBrand": "BMW",
    "carModel": "550d",
    "productionDate": "05/29/2015",
    "licensePlateNumber": "ARZ777",
    "carCategory": "luxury",
    "fuelType": "diesel",
    "description": "fast game",
    "image": "www.blahblah.com/url/something"
  },
  "user":{
        "email": "something@gmail.com"
    }
}'
*/

DROP PROCEDURE IF EXISTS dbo.usp_AddCar
GO

CREATE PROCEDURE dbo.usp_AddCar
	@json NVARCHAR(MAX)
AS
BEGIN
			INSERT dbo.CAR
			(
			   [user_id],
			   car_brand,   
			   car_model, 
			   production_date, 
			   license_plate_number, 
			   car_category_id , 
			   fuel_type_id,
			   text_description,
			   image_link 
			)

			SELECT
			   dbo.[USER].[user_id],
			   c.carBrand, 
			   c.carModel, 
			   c.productionDate , 
			   c.licensePlateNumber, 
			   dbo.CAR_CATEGORY.car_category_id, 
			   dbo.FUEL_TYPE.fuel_type_id,
			   c.[description],
			   c.[image]  
			
		    FROM OPENJSON(@json) 
			  WITH 
			 (
				email                 NVARCHAR(80)   '$.user.email',
				carBrand              NVARCHAR(20)   '$.car.carBrand',
				carModel              NVARCHAR(100)  '$.car.carModel',
				productionDate        DATETIME       '$.car.productionDate ',
				licensePlateNumber    NVARCHAR(10)   '$.car.licensePlateNumber',
				carCategory           NVARCHAR(10)   '$.car.carCategory',
				fuelType              NVARCHAR(10)   '$.car.fuelType',
				[description]         NVARCHAR(255)  '$.car.description',
				[image]               VARCHAR(500)   '$.car.image'
			 ) 
			  AS c 
			  JOIN dbo.[USER] ON c.email = dbo.[USER].email
			  JOIN dbo.CAR_CATEGORY ON c.carCategory   = dbo.CAR_CATEGORY.car_category_name
			  JOIN dbo.FUEL_TYPE ON c.fuelType = dbo.FUEL_TYPE.fuel_type_name
			  LEFT JOIN dbo.CAR ON c.licensePlateNumber = dbo.CAR.license_plate_number
		   WHERE dbo.CAR.license_plate_number is NULL
END
GO

