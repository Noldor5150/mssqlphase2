USE Carsharing
GO

/*
Execution example:

	exec usp_CreateOffer @json =N'{
  "offer": {
    "dateTime": "05/29/2021 - 06/29/2022",
    "description": "nice fast car good for a bank  robery",
    "pickUpAddress": "somewhere far beyond",
    "returnAddress": "closer",
    "priceEuHour": 25
  },
  "user": {
    "email": "something@gmail.com"
  },
  "car": {
    "licensePlateNumber": "ARZ777"
  }
}'
*/
DROP PROCEDURE IF EXISTS usp_CreateOffer
GO


CREATE PROCEDURE usp_CreateOffer
	@json NVARCHAR(MAX)
AS
BEGIN
          INSERT dbo.OFFER
	      (
               [user_id], 
               return_address,        
               pick_up_address,       
               date_time,             
               text_description,      
               price_EU_hour,       
               car_id 
          )

		  SELECT TOP 1
			   dbo.[USER].[user_id],
			   c.returnAddress, 
			   c.pickUpAddress, 
			   c.[dateTime], 
			   c.[description], 
			   c.priceEuHour,
			   dbo.CAR.car_id
			   
		  FROM OPENJSON(@json) 
			WITH 
			(
				email                 NVARCHAR (80)   '$.user.email',
				licensePlateNumber    NVARCHAR (10)   '$.car.licensePlateNumber',
				cardNumber            NVARCHAR (16)   '$.card.cardNumber',
			    [dateTime]            DATETIME        '$.offer.dateTime',
                [description]         NVARCHAR (250)  '$.offer.description',
                pickUpAddress         NVARCHAR (100)  '$.offer.pickUpAddress',
                returnAddress         NVARCHAR (100)  '$.offer.returnAddress',
                priceEuHour           SMALLINT        '$.offer.priceEuHour'

			)  AS c 
			JOIN dbo.[USER] ON c.email = dbo.[USER].email
			JOIN dbo.CAR ON c.licensePlateNumber   = dbo.CAR.license_plate_number
			JOIN dbo.[CARD] ON dbo.[USER].[user_id] = dbo.[CARD].[user_id]
		    LEFT JOIN dbo.OFFER ON dbo.[USER].[user_id] = dbo.OFFER.[user_id] 
				AND c.[dateTime] = dbo.OFFER.date_time 
				AND dbo.CAR.car_id = dbo.OFFER.car_id
		 WHERE dbo.OFFER.date_time is NULL; 
					
END
GO