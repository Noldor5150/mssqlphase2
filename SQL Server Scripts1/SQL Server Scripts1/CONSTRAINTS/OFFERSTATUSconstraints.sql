USE Carsharing;
GO


ALTER TABLE dbo.OFFER_STATUS
DROP CONSTRAINT IF EXISTS AK_OFFER_STATUS_offer_status_name ;
GO

ALTER TABLE dbo.OFFER_STATUS
ADD CONSTRAINT AK_OFFER_STATUS_offer_status_name
UNIQUE (offer_status_name);
GO