USE Carsharing;
GO

ALTER TABLE dbo.[CARD]
DROP CONSTRAINT IF EXISTS FK_CARD_USER_user_id;
GO

ALTER TABLE dbo.[CARD]
ADD CONSTRAINT FK_CARD_USER_userId
FOREIGN KEY ([user_id]) REFERENCES dbo.[USER] ([user_id]);
GO

ALTER TABLE dbo.[CARD]
DROP CONSTRAINT IF EXISTS FK_CARD_CARD_TYPE_card_type_id;
GO

ALTER TABLE dbo.[CARD]
ADD CONSTRAINT FK_CARD_CARD_TYPE_card_type_id
FOREIGN KEY (card_type_id) REFERENCES dbo.CARD_TYPE (card_type_id);
GO

ALTER TABLE dbo.[CARD]
DROP CONSTRAINT IF EXISTS AK_CARD_card_number;
GO

ALTER TABLE dbo.[CARD]
ADD CONSTRAINT AK_CARD_card_number
UNIQUE (card_number);
GO
