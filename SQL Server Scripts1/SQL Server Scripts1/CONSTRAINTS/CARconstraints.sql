USE Carsharing;
GO

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS FK_CAR_USER_user_id;
GO

ALTER TABLE dbo.CAR
ADD CONSTRAINT FK_CAR_USER_user_id
FOREIGN KEY ([user_id]) REFERENCES dbo.[USER] ([user_id]);
GO

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS AK_CAR_license_plate_number_user_id;
GO

ALTER TABLE dbo.CAR
ADD CONSTRAINT AK_CAR_license_plate_number_user_id
UNIQUE (license_plate_number,[user_id]);
GO

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS FK_CAR_CAR_CATEGORY_car_category_id;
GO

ALTER TABLE dbo.CAR
ADD CONSTRAINT FK_CAR_CAR_CATEGORY_car_category_id
FOREIGN KEY (car_category_id) REFERENCES dbo.CAR_CATEGORY (car_category_id);
GO

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS FK_CAR_FUEL_TYPE_fuel_type_id;
GO

ALTER TABLE dbo.CAR
ADD CONSTRAINT FK_CAR_FUEL_TYPE_fuel_type_id
FOREIGN KEY (fuel_type_id) REFERENCES dbo.FUEL_TYPE (fuel_type_id);
GO

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS DF_CAR_VALIDATION_default;
GO

ALTER TABLE dbo.CAR 
ADD CONSTRAINT [DF_CAR_VALIDATION_default]
DEFAULT dbo.ufn_GetValidationStatusIdByName('created') FOR validation_status_id;  
GO 

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS FK_CAR_VALIDATION_validation_status_id;
GO

ALTER TABLE dbo.CAR
ADD CONSTRAINT FK_CAR_VALIDATION_validation_status_id
FOREIGN KEY (validation_status_id) REFERENCES dbo.VALIDATION_STATUS (validation_status_id)

GO

ALTER TABLE dbo.CAR
DROP CONSTRAINT IF EXISTS FK_CAR_MODERATOR_moderator_id;
GO

ALTER TABLE dbo.CAR
ADD CONSTRAINT FK_CAR_MODERATOR_moderator_id
FOREIGN KEY (moderator_id) REFERENCES dbo.MODERATOR (moderator_id);
GO