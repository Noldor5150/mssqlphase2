USE Carsharing;
GO

IF object_id('dbo.ufn_getBoughtOfferStatusID', 'FN') is not null
    DROP FUNCTION dbo.ufn_getBoughtOfferStatusID
GO

CREATE FUNCTION dbo.ufn_getBoughtOfferStatusID
(
	
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.offer_status_id FROM dbo.OFFER_STATUS AS s
			WHERE s.offer_status_name = 'bought')
END
GO
