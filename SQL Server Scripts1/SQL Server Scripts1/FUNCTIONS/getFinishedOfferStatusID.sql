USE Carsharing;
GO

IF object_id('dbo.ufn_getFinishedOfferStatusID', 'FN') is not null
    DROP FUNCTION dbo.ufn_getFinishedOfferStatusID
GO

CREATE FUNCTION dbo.ufn_getFinishedOfferStatusID
(
	
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.offer_status_id FROM dbo.OFFER_STATUS AS s
			WHERE s.offer_status_name = 'finished')
END
GO
