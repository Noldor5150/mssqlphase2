USE Carsharing;
GO

IF object_id('dbo.ufn_getDeletedOfferStatusID', 'FN') is not null
    DROP FUNCTION dbo.ufn_getDeletedOfferStatusID
GO

CREATE FUNCTION dbo.ufn_getDeletedOfferStatusID
(
	
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.offer_status_id FROM dbo.OFFER_STATUS AS s
			WHERE s.offer_status_name = 'deleted')
END
GO
