USE Carsharing;
GO

IF object_id('dbo.ufn_getCreatedOfferStatusID', 'FN') is not null
    DROP FUNCTION dbo.ufn_getCreatedOfferStatusID
GO

CREATE FUNCTION dbo.ufn_getCreatedOfferStatusID
(
	
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.offer_status_id FROM dbo.OFFER_STATUS AS s
			WHERE s.offer_status_name = 'created')
END
GO
