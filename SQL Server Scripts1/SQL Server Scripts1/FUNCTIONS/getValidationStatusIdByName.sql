USE Carsharing;
GO

IF object_id('dbo.ufn_GetValidationStatusIdByName', 'FN') is not null
    DROP FUNCTION dbo.ufn_GetValidationStatusIdByName
GO

CREATE FUNCTION dbo.ufn_GetValidationStatusIdByName
(
	@StatusName VARCHAR(20)
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.validation_status_id  FROM dbo.VALIDATION_STATUS AS s
			WHERE s.validation_status_name = @StatusName)
END
GO
