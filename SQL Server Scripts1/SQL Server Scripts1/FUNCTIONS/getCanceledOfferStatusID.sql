




USE Carsharing;
GO

IF object_id('dbo.ufn_getCanceledOfferStatusID', 'FN') is not null
    DROP FUNCTION dbo.ufn_getCanceledOfferStatusID
GO

CREATE FUNCTION dbo.ufn_getCanceledOfferStatusID
(
	
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.offer_status_id FROM dbo.OFFER_STATUS AS s
			WHERE s.offer_status_name = 'canceled')
END
GO
