USE Carsharing;
GO

IF object_id('dbo.ufn_GetOfferStatusIdByName', 'FN') is not null
    DROP FUNCTION dbo.ufn_GetOfferStatusIdByName
GO

CREATE FUNCTION dbo.ufn_GetOfferStatusIdByName
(
	@StatusName VARCHAR(20)
)
RETURNS TINYINT
AS
BEGIN
	RETURN (SELECT s.offer_status_id FROM dbo.OFFER_STATUS AS s
			WHERE s.offer_status_name = @StatusName)
END
GO





