USE Carsharing
GO
WITH cte_OFFER_CAR_CATEGORY AS(
SELECT
      dbo.OFFER.date_time,
      dbo.OFFER.offer_id,
	  dbo.[USER].first_name,
      dbo.[USER].last_name,
      dbo.CAR_CATEGORY.car_category_name
    

 FROM 
      dbo.OFFER
      JOIN dbo.[USER] on dbo.OFFER.[user_id] = dbo.[USER].[user_id]
      JOIN dbo.CAR on dbo.[OFFER].[car_id] = dbo.CAR.[user_id]
      JOIN dbo.CAR_CATEGORY on dbo.CAR_CATEGORY.[car_category_id] = dbo.CAR.car_category_id
 

 WHERE dbo.OFFER.date_time BETWEEN '2021-01-01' AND '2021-12-31 23:59:59.997'
 --ORDER BY dbo.OFFER.date_time
 --GROUP BY dbo.CAR_CATEGORY.car_category_name
 )

 SELECT  
       car_category_name, COUNT (car_category_name) AS car_categorys_count
 FROM 
      cte_OFFER_CAR_CATEGORY
 WHERE date_time BETWEEN'2021-06-01' AND '2021-06-30 23:59:59.997'
 GROUP BY 
      car_category_name
 ORDER BY 
       car_categorys_count DESC