SELECT
       CAST(DATEPART(YEAR,dbo.OFFER.date_time) AS VARCHAR (10)) +'.'+ CAST(DATEPART(MONTH,dbo.OFFER.date_time) AS VARCHAR (10)),
      dbo.OFFER.offer_id,
	  dbo.[USER].first_name,
      dbo.[USER].last_name,
      dbo.CAR_CATEGORY.car_category_name
    

 FROM 
      dbo.OFFER
      JOIN dbo.[USER] on dbo.OFFER.[user_id] = dbo.[USER].[user_id]
      JOIN dbo.CAR on dbo.[OFFER].[car_id] = dbo.CAR.[car_id]
      JOIN dbo.CAR_CATEGORY on dbo.CAR_CATEGORY.[car_category_id] = dbo.CAR.car_category_id
 

 --WHERE dbo.OFFER.date_time BETWEEN '2021-01-07' AND '2021-08-07 23:59:59.997'
 --ORDER BY dbo.OFFER.date_time
 --GROUP BY dbo.CAR_CATEGORY.car_category_name